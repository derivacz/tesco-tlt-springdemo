truncate table users;

insert into users (id, name, surname, itime)

select *
from (
       WITH numbers AS (
         SELECT * FROM generate_series(1, 100000)
         )
         SELECT generate_series, MD5(random()::text) as name, MD5(random()::text) as surname, now() as itime

         FROM numbers
     ) as dt;
