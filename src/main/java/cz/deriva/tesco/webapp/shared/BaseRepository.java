package cz.deriva.tesco.webapp.shared;

import java.util.Optional;

public interface BaseRepository<Type> {

  Optional<Type> findById(final Long id);

  void save(final Type domainModelObject);

}
