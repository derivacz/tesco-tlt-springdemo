package cz.deriva.tesco.webapp.shared;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ExceptionCodeType {

  EMPTY_VALUE("empty_value"),
  INVALID_FIELD("invalid_field");

  private String type;

  ExceptionCodeType(String type) {
    this.type = type;
  }

  @JsonValue
  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return this.type;
  }


}
