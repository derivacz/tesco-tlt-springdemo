package cz.deriva.tesco.webapp.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootConfiguration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {

  private static Logger LOGGER = LogManager.getLogger(MvcConfig.class);

}
