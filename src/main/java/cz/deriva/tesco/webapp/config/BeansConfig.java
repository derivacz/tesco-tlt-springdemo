package cz.deriva.tesco.webapp.config;

import cz.deriva.tesco.webapp.domain.user.application.UserCommandDbService;
import cz.deriva.tesco.webapp.domain.user.application.UserCommandService;
import cz.deriva.tesco.webapp.domain.user.application.UserCommandValidatorDecorator;
import cz.deriva.tesco.webapp.domain.user.persistence.UserJpaRepository;
import cz.deriva.tesco.webapp.domain.user.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootConfiguration
@EnableAutoConfiguration
@EnableTransactionManagement
@ComponentScan("cz.deriva.tesco")
@EnableJpaRepositories("cz.deriva.tesco")
@EntityScan("cz.deriva.tesco")
public class BeansConfig {

  @Bean
  public UserCommandService userCommandService(
      final UserJpaRepository userJpaRepository,
      final UserRepository userRepository
  ) {

    final UserCommandDbService decoratee = new UserCommandDbService(userJpaRepository, userRepository);
    return new UserCommandValidatorDecorator(decoratee);

  }

}
