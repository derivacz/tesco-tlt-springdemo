package cz.deriva.tesco.webapp.domain.user.repository;

import cz.deriva.tesco.webapp.shared.BaseRepository;
import cz.deriva.tesco.webapp.domain.user.model.User;

/**
 * Created by jirka on 2019-11-04.
 *
 * @author Jiri Nemec
 */
public interface UserRepository extends BaseRepository<User> {
}
