package cz.deriva.tesco.webapp.domain.user.model;

import cz.deriva.commons.AssertUtils;

import java.time.LocalDateTime;

public class User {

  private final long id;
  private final String name;
  private final String surname;
  private final LocalDateTime itime;

  public User(
      final Long id,
      final String name,
      final String surname,
      final LocalDateTime itime
  ) {

    AssertUtils.notNull(id, "id");
    this.id = id;

    AssertUtils.notBlank(name, "name");
    this.name = name;

    AssertUtils.notBlank(surname, "surname");
    this.surname = surname;

    AssertUtils.notNull(itime, "itime");
    this.itime = itime;

  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public LocalDateTime getItime() {
    return itime;
  }

}
