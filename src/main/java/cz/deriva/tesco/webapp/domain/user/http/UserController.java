package cz.deriva.tesco.webapp.domain.user.http;


import cz.deriva.commons.AssertUtils;
import cz.deriva.tesco.webapp.domain.user.application.UserCommandService;
import cz.deriva.tesco.webapp.domain.user.application.UserQueryService;
import cz.deriva.tesco.webapp.domain.user.converter.UserAssembler;
import cz.deriva.tesco.webapp.domain.user.converter.UserDto;
import cz.deriva.tesco.webapp.domain.user.model.User;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

  private final UserQueryService userQueryService;
  private final UserCommandService userCommandService;

  public UserController(final UserQueryService userQueryService, UserCommandService userCommandService) {

    AssertUtils.notNull(userCommandService, "userCommandService");
    this.userCommandService = userCommandService;

    AssertUtils.notNull(userQueryService, "userQueryService");
    this.userQueryService = userQueryService;

  }

  @PostMapping(value = "/")
  public @ResponseBody
  UserDto create(@RequestBody UserDto command) {

    final User create = userCommandService.create(command);
    final UserAssembler ass = new UserAssembler();

    return ass.toDto(create);

  }


  @GetMapping(value = "/spring-jdbc-template/")
  public @ResponseBody
  List<UserDto> jdbcTemplate() {

    final List<User> all = userQueryService.getAllByJdbcTemplate();

    final ArrayList<UserDto> res = new ArrayList<>();
    final UserAssembler ass = new UserAssembler();

    for (User user : all) {
      res.add(ass.toDto(user));
    }

    return res;

  }

  @GetMapping(value = "/spring-data-jpa/")
  public @ResponseBody
  List<UserDto> springJpa() {

    final List<User> all = userQueryService.getAllBySpringDataJpa();

    final ArrayList<UserDto> res = new ArrayList<>();
    final UserAssembler ass = new UserAssembler();

    for (User user : all) {
      res.add(ass.toDto(user));
    }

    return res;

  }

}
