package cz.deriva.tesco.webapp.domain.user.converter;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.deriva.tesco.webapp.shared.BaseDto;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class UserDto extends BaseDto {

  @JsonProperty("name")
  private String name;

  @JsonProperty("surname")
  private String surname;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }

}
