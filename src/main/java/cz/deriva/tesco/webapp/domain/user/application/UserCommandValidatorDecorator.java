package cz.deriva.tesco.webapp.domain.user.application;

import cz.deriva.commons.AssertUtils;
import cz.deriva.tesco.webapp.domain.user.converter.UserDto;
import cz.deriva.tesco.webapp.domain.user.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import java.time.LocalDateTime;


public class UserCommandValidatorDecorator implements UserCommandService {

  private static Logger LOGGER = LogManager.getLogger(UserCommandValidatorDecorator.class);

  private final UserCommandService decoratee;

  public UserCommandValidatorDecorator(final UserCommandService userCommandService) {

    AssertUtils.notNull(userCommandService, "decoratee");
    this.decoratee = userCommandService;

  }

  @Override
  public User create(final UserDto user) {

    final Errors errors = new BeanPropertyBindingResult(user, "user");

    final UserValidator validator = new UserValidator();
    validator.validate(user, errors);

    if (errors.hasErrors()) {
      LOGGER.info("Validacni chyby pri vlozeni user: {}", errors);
      throw new IllegalArgumentException(String.format("Nevalidni data: %s", user));
    }

    if(user.getItime() == null){
      user.setItime(LocalDateTime.now());
    }

    return decoratee.create(user);

  }

}
