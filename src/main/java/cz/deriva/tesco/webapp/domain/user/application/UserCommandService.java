package cz.deriva.tesco.webapp.domain.user.application;

import cz.deriva.tesco.webapp.domain.user.converter.UserDto;
import cz.deriva.tesco.webapp.domain.user.model.User;


public interface UserCommandService {

  User create(final UserDto user);

}
