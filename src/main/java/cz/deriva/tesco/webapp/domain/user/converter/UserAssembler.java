package cz.deriva.tesco.webapp.domain.user.converter;

import cz.deriva.commons.AssertUtils;
import cz.deriva.tesco.webapp.domain.user.model.User;
import cz.deriva.tesco.webapp.domain.user.persistence.UserEntity;


public class UserAssembler {

  public User toDomainObject(final UserEntity userEntity) {

    AssertUtils.notNull(userEntity, "userEntity");

    final User user = new User(
        userEntity.getId(),
        userEntity.getName(),
        userEntity.getSurname(),
        userEntity.getItime()
    );

    return user;

  }

  public UserDto toDto(final User user) {

    AssertUtils.notNull(user, "user");

    final UserDto dto = new UserDto();

    dto.setId(user.getId());
    dto.setName(user.getName());
    dto.setSurname(user.getSurname());
    dto.setItime(user.getItime());

    return dto;

  }

  public User toDomainObject(final UserDto user) {

    AssertUtils.notNull(user, "user");


    final User result = new User(

        user.getId(),
        user.getName(),
        user.getSurname(),
        user.getItime()
    );

    return result;

  }

  public UserEntity toEntity(User domainModelObject) {

    final UserEntity ent = new UserEntity();

    ent.setId(domainModelObject.getId());
    ent.setName(domainModelObject.getName());
    ent.setSurname(domainModelObject.getSurname());
    ent.setItime(domainModelObject.getItime());

    return ent;

  }

}
