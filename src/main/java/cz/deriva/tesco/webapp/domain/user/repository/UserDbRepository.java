package cz.deriva.tesco.webapp.domain.user.repository;

import cz.deriva.commons.AssertUtils;
import cz.deriva.tesco.webapp.domain.user.converter.UserAssembler;
import cz.deriva.tesco.webapp.domain.user.model.User;
import cz.deriva.tesco.webapp.domain.user.persistence.UserEntity;
import cz.deriva.tesco.webapp.domain.user.persistence.UserJpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDbRepository implements UserRepository {

  private final UserJpaRepository userJpaRepository;

  public UserDbRepository(UserJpaRepository userJpaRepository) {

    AssertUtils.notNull(userJpaRepository, "userJpaRepository");
    this.userJpaRepository = userJpaRepository;

  }

  @Override
  public Optional<User> findById(Long id) {

    final Optional<UserEntity> ent = userJpaRepository.findById(id);

    if (!ent.isPresent()) {
      return Optional.empty();
    }

    final UserAssembler ass = new UserAssembler();

    return Optional.of(ass.toDomainObject(ent.get()));

  }

  @Override
  public void save(final User domainModelObject) {

    final UserAssembler ass = new UserAssembler();

    final UserEntity userEntity = ass.toEntity(domainModelObject);

    userJpaRepository.save(userEntity);

  }

}
