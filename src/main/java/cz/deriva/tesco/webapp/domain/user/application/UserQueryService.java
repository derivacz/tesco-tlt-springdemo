package cz.deriva.tesco.webapp.domain.user.application;

import cz.deriva.commons.AssertUtils;
import cz.deriva.tesco.webapp.domain.user.converter.UserAssembler;
import cz.deriva.tesco.webapp.domain.user.model.User;
import cz.deriva.tesco.webapp.domain.user.persistence.UserEntity;
import cz.deriva.tesco.webapp.domain.user.persistence.UserJpaRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserQueryService {

  private static Logger LOGGER = LogManager.getLogger(UserQueryService.class);

  private final UserJpaRepository userJpaRepository;
  private final JdbcTemplate jdbcTemplate;

  public UserQueryService(
      final UserJpaRepository userJpaRepository,
      final JdbcTemplate jdbcTemplate
  ) {

    AssertUtils.notNull(userJpaRepository, "userJpaRepository");
    this.userJpaRepository = userJpaRepository;

    AssertUtils.notNull(jdbcTemplate, "jdbcTemplate");
    this.jdbcTemplate = jdbcTemplate;

  }

  public List<User> getAllByJdbcTemplate() {

    final String sql = "SELECT id, name, surname, itime FROM users";

    final StopWatch sw = new StopWatch();
    sw.start();

    final List<User> all = jdbcTemplate.query(sql, (RowMapper) (rs, rowNum) -> new User(
        rs.getLong("id"),
        rs.getString("name"),
        rs.getString("surname"),
        rs.getTimestamp("itime").toLocalDateTime()
    ));

    sw.stop();
    LOGGER.info("Time getAllByJdbcTemplate: {} ms", sw.getTotalTimeMillis());

    return all;

  }

  public List<User> getAllBySpringDataJpa() {

    final StopWatch sw = new StopWatch();
    sw.start();

    final List<UserEntity> all = userJpaRepository.findAll();

    sw.stop();
    LOGGER.info("Time getAllBySpringDataJpa: {} ms", sw.getTotalTimeMillis());

    final UserAssembler ass = new UserAssembler();

    final ArrayList<User> result = new ArrayList<>();

    for (UserEntity entity : all) {
      result.add(ass.toDomainObject(entity));
    }

    return result;

  }

}
