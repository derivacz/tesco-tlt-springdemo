package cz.deriva.tesco.webapp.domain.user.application;

import cz.deriva.commons.AssertUtils;
import cz.deriva.tesco.webapp.domain.user.converter.UserAssembler;
import cz.deriva.tesco.webapp.domain.user.converter.UserDto;
import cz.deriva.tesco.webapp.domain.user.model.User;
import cz.deriva.tesco.webapp.domain.user.persistence.UserJpaRepository;
import cz.deriva.tesco.webapp.domain.user.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class UserCommandDbService implements UserCommandService {

  private static Logger LOGGER = LogManager.getLogger(UserCommandDbService.class);

  private final UserJpaRepository userJpaRepository;
  private final UserRepository userRepository;

  public UserCommandDbService(
      final UserJpaRepository userJpaRepository,
      final UserRepository userRepository
  ) {

    AssertUtils.notNull(userRepository, "userRepository");
    this.userRepository = userRepository;

    AssertUtils.notNull(userJpaRepository, "userJpaRepository");
    this.userJpaRepository = userJpaRepository;

  }


  @Override
  public User create(final UserDto user) {

    AssertUtils.notNull(user, "user");

    final UserAssembler ass = new UserAssembler();
    final User res = ass.toDomainObject(user);

    userRepository.save(res);

    return res;

  }

}
