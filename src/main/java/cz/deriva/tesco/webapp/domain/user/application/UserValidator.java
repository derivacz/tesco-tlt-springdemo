package cz.deriva.tesco.webapp.domain.user.application;

import cz.deriva.commons.StringUtils;
import cz.deriva.tesco.webapp.shared.ExceptionCodeType;
import cz.deriva.tesco.webapp.domain.user.converter.UserDto;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {


  @Override
  public boolean supports(Class<?> clazz) {
    return UserDto.class.isAssignableFrom(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {

    final UserDto user = (UserDto) target;

    if (user.getId() == null || user.getId() <= 0) {
      errors.rejectValue("id", ExceptionCodeType.INVALID_FIELD.toString());
    }

    if (StringUtils.isBlank(user.getName())) {
      errors.rejectValue("name", ExceptionCodeType.EMPTY_VALUE.toString());
    }

    if (StringUtils.isBlank(user.getSurname())) {
      errors.rejectValue("surname", ExceptionCodeType.EMPTY_VALUE.toString());
    }

  }

}
